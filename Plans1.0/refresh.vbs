Set Arg = WScript.Arguments

If Arg.Count = 1 Then 
   Dim strPath

   '���� � �����
   strPath = Arg(0)

   UpdateScheme (strPath)

End if 

Function UpdateScheme(strPath)
   Dim visApp
   Set visApp = WScript.CreateObject("Visio.InvisibleApp")

   visApp.AlertResponse = 1
   visApp.Documents.Open strPath
   visApp.ActiveWindow.SelectAll
   
   For Each shp In visApp.ActiveWindow.Selection
      If shp.CellExists("User.ODBCConnection", 0) Then
         If shp.Cells("EventDrop").Formula = "RUNADDON(""DBR"")" Then
            shp.Cells("EventDrop").Trigger
         End If
      End If
   Next

   if visApp.ActiveDocument.Saved=false Then visApp.ActiveDocument.Save
   visApp.Quit
End Function