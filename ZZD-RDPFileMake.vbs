On Error Resume Next 
Set WShShell = WScript.CreateObject("WScript.Shell")
Set m_FSO = CreateObject("Scripting.FileSystemObject")
'������� ������������ � AD � ��������� ��� ���������
Set objSysInfo = CreateObject("ADSystemInfo")
ADSPath = "LDAP://" & objSysInfo.UserName
Set objUser = GetObject(ADSPath)
ShortUserName = objUser.SamAccountName		'������� �������� ������� �� AD
DomainName = objSysInfo.DomainShortName		'������� �������� ������� �� AD

' ������ ���� � �������� �����
DesktopPath = WShShell.SpecialFolders("Desktop")

arrMemberOf = objUser.memberOf
Dim UserGroups
Dim objGroup
UserGroups=""

GroupPath = ""
DoRDP = False
sGroup = "ZZD-Term1CUsers-LS-A"
For Each key In arrMemberOf
	If InStr(key, sGroup) Then
		'UserGroups=UserGroups & "[" & key & "]"
		GroupPath = key
		DoRDP = True
	End If
Next


If DoRDP Then
ADSGroup = "LDAP://" & GroupPath
Set objGroup = GetObject (ADSGroup)

sFileName = objGroup.extensionAttribute1
sName = objGroup.extensionAttribute2
sPath = DesktopPath & "\" & sFileName & ".rdp"

' ��������� ������� ������ �� ����� - ���� ���� - ������� ���'
If m_FSO.FileExists(sPath) or m_FSO.FolderExists(sPath) Then
	m_FSO.DeleteFile (sPath),1
End If

Set RDPFile = m_FSO.CreateTextFile (sPath, True)
RDPFile.writeline ("screen mode id:i:2")
RDPFile.writeline ("use multimon:i:0")
RDPFile.writeline ("desktopwidth:i:1280")
RDPFile.writeline ("desktopheight:i:1024")
RDPFile.writeline ("session bpp:i:16")
RDPFile.writeline ("winposstr:s:0,1,0,0,800,600")
RDPFile.writeline ("compression:i:1")
RDPFile.writeline ("keyboardhook:i:2")
RDPFile.writeline ("audiocapturemode:i:0")
RDPFile.writeline ("videoplaybackmode:i:1")
RDPFile.writeline ("connection type:i:2")
RDPFile.writeline ("displayconnectionbar:i:1")
RDPFile.writeline ("disable wallpaper:i:1")
RDPFile.writeline ("disable full window drag:i:1")
RDPFile.writeline ("allow desktop composition:i:0")
RDPFile.writeline ("allow font smoothing:i:0")
RDPFile.writeline ("disable menu anims:i:1")
RDPFile.writeline ("disable themes:i:1")
RDPFile.writeline ("disable cursor setting:i:0")
RDPFile.writeline ("bitmapcachepersistenable:i:1")
'���������� ����� �������
RDPFile.writeline ("full address:s:" & sName)
RDPFile.writeline ("audiomode:i:2")
RDPFile.writeline ("redirectprinters:i:1")
RDPFile.writeline ("redirectcomports:i:0")
RDPFile.writeline ("redirectsmartcards:i:0")
RDPFile.writeline ("redirectclipboard:i:1")
RDPFile.writeline ("redirectposdevices:i:0")
RDPFile.writeline ("redirectdirectx:i:1")
RDPFile.writeline ("autoreconnection enabled:i:1")
RDPFile.writeline ("authentication level:i:0")
RDPFile.writeline ("prompt for credentials:i:0")
RDPFile.writeline ("negotiate security layer:i:1")
RDPFile.writeline ("remoteapplicationmode:i:0")
RDPFile.writeline ("alternate shell:s:")
RDPFile.writeline ("shell working directory:s:")
RDPFile.writeline ("gatewayhostname:s:")
RDPFile.writeline ("gatewayusagemethod:i:4")
RDPFile.writeline ("gatewaycredentialssource:i:4")
RDPFile.writeline ("gatewayprofileusagemethod:i:0")
RDPFile.writeline ("promptcredentialonce:i:1")
'���������� ����� ������������ � ������� Domain\Username
RDPFile.writeline ("username:s:" & DomainName& "\"& ShortUserName)
RDPFile.writeline ("drivestoredirect:s:")
RDPFile.close
End If

WScript.Quit