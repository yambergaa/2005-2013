ON ERROR RESUME NEXT  
Dim strUserDN, objSysInfo  
 
set WshShell = CreateObject("WScript.Shell")  
Set WshNetwork = WScript.CreateObject("WScript.Network")  
Set objSysInfo = CreateObject("ADSystemInfo")  
 
LogonServer=WshShell.ExpandEnvironmentStrings("%LogonServer%")  
strUserDN = objSysInfo.userName  
Set UserObj = GetObject("LDAP://" & strUserDN)  
 
Dim UserGroups  
Dim GroupObj  
UserGroups=""  
 
For Each GroupObj In UserObj.Groups  
        UserGroups=UserGroups & "[" & GroupObj.Name & "]"  
Next  
 
'-----------����� �������� ������ �������-----------

'MapDrv "V:", "\\10.10.0.252\docs_new"
MapDrv "V:", "\\172.25.188.5\OD"

if InGroup("CN=ZZD-IT-LS-A") then  
        	'MapDrv "T:", "\\10.10.0.33\distrib$"
		'MapDrv "Z:", "\\10.10.0.251\video"
		'MapDrv "Y:", "\\10.10.99.99\video\"
		MapPrint "\\ZZD-WS184014\zzd-416-l-02"
		MapPrint "\\ZZD-WS184015\zzd-416-l-01"
end if  


if InGroup("CN=ZZD-ArchiveProject-LS-A") then  
	MapDrv "W:", "\\172.25.188.16\arhive"
end if  


if InGroup("CN=ZZD-RzdpDocs-LS-A") then  
	MapDrv "X:", "\\172.25.188.1\moskow_doc"
end if

'-----����
if InGroup("CN=kodeks") then  
	MapDrv "K:", "\\172.25.188.13\kodeks"
end if


'-----��� �������
if InGroup("CN=kodeks2") then  
	MapDrv "S:", "\\172.25.188.13\kodeks\texpert\TEXPERT"
end if


'-----�����-������
if InGroup("CN=kodeks3") then  
	MapDrv "O:", "\\172.25.188.13\kodeks\resurs\"
end if


'-----����������� ����
if InGroup("CN=ZZD-KPlus-LS-A") then  
	MapDrv "U:", "\\172.25.188.13\Veda3000"
end if


'---------------------------------------------------

Function MapPrint(strPath)
	WSHNetwork.AddWindowsPrinterConnection strPath
	WSHNetwork.SetDefaultPrinter strPath
End Function

Function InGroup(strGroup)  
        InGroup=False  
        If InStr(UserGroups,"[" & strGroup & "]") Then  
                InGroup=True  
        End If  
End Function

 
Function MapDrv(DrvLet, UNCPath)  
 
    Dim WshNetwork,objFSO          ' Object variable  
    Dim Msg  
 
    Set WshNetwork = WScript.CreateObject("WScript.Network")  
    Set objFSO = CreateObject("Scripting.FileSystemObject")
 
    On Error Resume Next  
 
    If objFSO.DriveExists(DrvLet) Then  
        WshNetwork.RemoveNetworkDrive DrvLet  
    End If
 
    WshNetwork.MapNetworkDrive DrvLet, UNCPath  
     
    Select Case Err.Number  
        Case 0            ' No error  
 
        Case -2147023694  
            WshNetwork.RemoveNetworkDrive DrvLet  
            WshNetwork.MapNetworkDrive DrvLet, UNCPath  
               
        Case -2147024811  
            WshNetwork.RemoveNetworkDrive DrvLet  
            WshNetwork.MapNetworkDrive DrvLet, UNCPath  
 
        Case Else  
 
            Msg = "Mapping network drive error: " & _  
                   CStr(Err.Number) & " 0x" & Hex(Err.Number) & vbCrLf & _  
                  "Error description: " & Err.Description & vbCrLf  
            Msg = Msg & "Domain: " & WshNetwork.UserDomain & vbCrLf  
            Msg = Msg & "Computer Name: " & WshNetwork.ComputerName & vbCrLf  
            Msg = Msg & "User Name: " & WshNetwork.UserName & vbCrLf & vbCrLf  
            Msg = Msg & "Device name: " & DrvLet & vbCrLf  
            Msg = Msg & "Map path: " & UNCPath  
 
            WshShell.LogEvent 1, Msg, "\\SRV"
    End Select  
End Function 